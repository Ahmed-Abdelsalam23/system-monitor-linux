#include "linux_parser.h"

#include <dirent.h>
#include <unistd.h>

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <numeric>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

using std::stof;
using std::string;
using std::to_string;
using std::vector;

// DONE: An example of how to read data from the filesystem
string LinuxParser::OperatingSystem() {
  string line;
  std::ifstream filestream(kOSPath);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      size_t equals_pos = line.find('=');
      if (equals_pos != std::string::npos) {
        std::string key = line.substr(0, equals_pos);
        if (key == "PRETTY_NAME") {
          std::string value =
              line.substr(equals_pos + 2, line.length() - equals_pos - 3);
          return value;
        }
      }
    }
  }
  return "";
}

// DONE: An example of how to read data from the filesystem
string LinuxParser::Kernel() {
  string os, version, kernel;
  string line;
  std::ifstream stream(kProcDirectory + kVersionFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> os >> version >> kernel;
  }
  return kernel;
}

vector<int> LinuxParser::Pids() {
  vector<int> pids;
  DIR* directory = opendir(kProcDirectory.c_str());
  struct dirent* file;
  while ((file = readdir(directory)) != nullptr) {
    // Is this a directory?
    if (file->d_type == DT_DIR) {
      // Is every character of the name a digit?
      string filename(file->d_name);
      if (std::all_of(filename.begin(), filename.end(), isdigit)) {
        int pid = stoi(filename);
        pids.push_back(pid);
      }
    }
  }
  closedir(directory);
  return pids;
}

// Read and return the system memory utilization
float LinuxParser::MemoryUtilization() {
  std::unordered_map<std::string, float> memoryInfo;
  std::ifstream stream(kProcDirectory + kMeminfoFilename);
  std::string line;

  if (stream.is_open()) {
    while (std::getline(stream, line)) {
      std::istringstream linestream(line);
      std::string key;
      float value;
      if (linestream >> key >> value) {
        // Remove ':' from key
        key.pop_back();
        memoryInfo[key] = value;
      }
    }
  }

  // Calculate memory utilization
  // Ensure MemTotal and MemFree are present to avoid division by zero
  float memTotal = memoryInfo["MemTotal"];
  float memFree = memoryInfo["MemFree"];
  return memTotal != 0 ? (memTotal - memFree) / memTotal : 0;
}

// Read and return the system uptime
long LinuxParser::UpTime() {
  std::ifstream stream(kProcDirectory + kUptimeFilename);
  long uptime = 0;
  if (stream.is_open()) {
    stream >> uptime;
    // No need to manually close the ifstream; it will be closed when going out
    // of scope
  }
  return uptime;
}

// Read and return the number of jiffies for the system
long LinuxParser::Jiffies() {
  auto values = LinuxParser::CpuUtilization();  // Use auto for type deduction
  std::vector<long> valuesLong(
      values.size());  // Directly use the size of values

  // Transform string vector to long vector using std::transform and a lambda
  // function
  std::transform(values.begin(), values.end(), valuesLong.begin(),
                 [](const std::string& val) -> long { return std::stol(val); });

  // List of CPU states to consider, ensuring not to include guest values as per
  // original intent
  std::vector<CPUStates> all = {kUser_,   kNice_, kSystem_,  kIdle_,
                                kIOwait_, kIRQ_,  kSoftIRQ_, kSteal_};

  // Calculate the total using std::accumulate and a lambda that filters based
  // on 'all' states
  long total = std::accumulate(
      valuesLong.begin(), valuesLong.end(), 0L,
      [&](long sum, const long& val) -> long {
        auto pos =
            &val - &valuesLong[0];  // Find the index of val in valuesLong
        if (std::find(all.begin(), all.end(), pos) !=
            all.end()) {  // Check if the index is in 'all'
          return sum + val;
        }
        return sum;
      });

  return total;
}

// Read and return the number of active jiffies for a PID
long LinuxParser::ActiveJiffies(int pid) {
  std::string line;
  std::vector<std::string> values;
  std::ifstream stream(kProcDirectory + std::to_string(pid) + kStatFilename);

  if (stream.is_open() && std::getline(stream, line)) {
    std::istringstream linestream(line);
    std::string value;
    while (linestream >> value) {
      values.push_back(value);
    }
  }

  if (values.size() > 14) {
    long utime = std::stol(values[13]);
    long stime = std::stol(values[14]);
    return utime + stime;  // Correctly summing the utime and stime
  }

  return 0;
}

// Read and return the number of active jiffies for the system
long LinuxParser::ActiveJiffies() {
  auto jiffies = CpuUtilization();
  return std::stol(jiffies[CPUStates::kUser_]) +
         std::stol(jiffies[CPUStates::kNice_]) +
         std::stol(jiffies[CPUStates::kSystem_]) +
         std::stol(jiffies[CPUStates::kIRQ_]) +
         std::stol(jiffies[CPUStates::kSoftIRQ_]) +
         std::stol(jiffies[CPUStates::kSteal_]);
}

// Read and return the number of idle jiffies for the system
long LinuxParser::IdleJiffies() {
  auto jiffies = CpuUtilization();
  return std::stol(jiffies[CPUStates::kIdle_]) +
         std::stol(jiffies[CPUStates::kIOwait_]);
}

// Read and return CPU utilization
std::vector<std::string> LinuxParser::CpuUtilization() {
  std::string line;
  std::vector<std::string> values;
  std::ifstream stream(kProcDirectory + kStatFilename);

  if (stream.is_open() && std::getline(stream, line)) {
    std::istringstream linestream(line);
    std::string key;
    linestream >> key;  // Discard the first token ('cpu')
    std::copy(std::istream_iterator<std::string>(linestream),
              std::istream_iterator<std::string>(), std::back_inserter(values));
  }
  return values;
}

// Read and return the total number of processes
int LinuxParser::TotalProcesses() {
  std::ifstream stream(kProcDirectory + kStatFilename);
  std::string line, key;

  while (stream.is_open() && std::getline(stream, line)) {
    std::istringstream linestream(line);
    linestream >> key;
    if (key == "processes") {
      int totalProcesses;
      linestream >> totalProcesses;
      return totalProcesses;
    }
  }
  return 0;
}

// Read and return the number of running processes
int LinuxParser::RunningProcesses() {
  std::ifstream stream(kProcDirectory + kStatFilename);
  std::string line, key;

  while (stream.is_open() && std::getline(stream, line)) {
    std::istringstream linestream(line);
    linestream >> key;
    if (key == "procs_running") {
      int runningProcesses;
      linestream >> runningProcesses;
      return runningProcesses;
    }
  }
  return 0;
}

// Read and return the command associated with a process
std::string LinuxParser::Command(int pid) {
  std::ifstream stream(kProcDirectory + std::to_string(pid) + kCmdlineFilename);
  std::string command;
  if (stream) {
    std::getline(stream, command);
  }
  return command;
}

// Read and return the memory used by a process
std::string LinuxParser::Ram(int pid) {
  std::ifstream stream(kProcDirectory + std::to_string(pid) + kStatusFilename);
  std::string line, key, value;
  while (std::getline(stream, line)) {
    std::istringstream linestream(line);
    linestream >> key;
    if (key == "VmSize:") {
      linestream >> value;
      long memory_kb = std::stol(value) / 1024;  // Convert from KB to MB
      return std::to_string(memory_kb);
    }
  }
  return "0";
}

// Read and return the user ID associated with a process
std::string LinuxParser::Uid(int pid) {
  std::ifstream stream(kProcDirectory + std::to_string(pid) + kStatusFilename);
  std::string line, key, value;
  while (std::getline(stream, line)) {
    std::istringstream linestream(line);
    while (linestream >> key >> value) {
      if (key == "Uid:") {
        return value;
      }
    }
  }
  return "0";
}

// Read and return the user associated with a process
std::string LinuxParser::User(int pid) {
  std::string uid = LinuxParser::Uid(pid);
  std::ifstream stream(kPasswordPath);
  std::string line, name, x, uidValue;
  while (std::getline(stream, line)) {
    std::replace(line.begin(), line.end(), ':', ' ');
    std::istringstream linestream(line);
    while (linestream >> name >> x >> uidValue) {
      if (uidValue == uid) {
        return name;
      }
    }
  }
  return "unknown";
}
// Read and return the uptime of a process
long LinuxParser::UpTime(int pid) {
  std::ifstream stream(kProcDirectory + std::to_string(pid) + kStatFilename);
  std::string line;
  if (std::getline(stream, line)) {
    std::istringstream linestream(line);
    std::vector<std::string> values(
        std::istream_iterator<std::string>{linestream},
        std::istream_iterator<std::string>{});
    if (values.size() > 21) {
      long startTimeTicks = std::stol(values[21]);
      long systemUpTime = LinuxParser::UpTime();
      long ticksPerSecond = sysconf(_SC_CLK_TCK);
      return systemUpTime - (startTimeTicks / ticksPerSecond);
    }
  }
  return 0;
}
